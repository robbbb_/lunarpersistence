/*
  DigitalReadSerial
 Reads a digital input on pin 2, prints the result to the serial monitor 
 
 This example code is in the public domain.
 */
 
const int highSwitchPin = 3;//guess!
const int lowSwitchPin = 4;
const int  limitSwitchGroundPin = 5;
int samplePeriodMillis;

void setup()
{
  Serial.begin(9600);// start serial for debug output
  //debugSetup();//start serial, print header
//  pinMode(highSwitchPin, INPUT);
//  digitalWrite(highSwitchPin, HIGH); //Explicit pullups
//  pinMode(lowSwitchPin, INPUT);
//  digitalWrite(lowSwitchPin, HIGH); 
  pinMode(highSwitchPin, INPUT_PULLUP);
  pinMode(lowSwitchPin, INPUT_PULLUP);


}

// the loop routine runs over and over again forever:
void loop() {
  // read the input pin:
  int highSwitchPinState = digitalRead(highSwitchPin);
  int lowSwitchPinState = digitalRead(lowSwitchPin);
  // print out the state of the button:
  Serial.print("low:\t");
  Serial.print(lowSwitchPinState);
  Serial.print("\thi:\t");
  Serial.println(highSwitchPinState);
  delay(99);        // delay in between reads for stability
}



