void getHeading() 
{
  sensors_event_t event; 
  /* Get a new sensor event */
    if(!mag.begin())
  {
      if(debugSerial){
    Serial.println("No Compass!");
      }
    //the code never gets here. Ever. This is a bug.
  }
  else{
  mag.getEvent(&event);
  
  /* Display the results (magnetic vector values are in micro-Tesla (uT)) */
  //  Serial.print("X: "); Serial.print(event.magnetic.x); Serial.print("  ");
  //  Serial.print("Y: "); Serial.print(event.magnetic.y); Serial.print("  ");
  //  Serial.print("Z: "); Serial.print(event.magnetic.z); Serial.print("  ");Serial.println("uT");

  // Hold the module so that Z is pointing 'up' and you can measure the heading with x&y
  // Calculate heading when the magnetometer is level, then correct for signs of axis.
  float heading = atan2(event.magnetic.y, event.magnetic.x);

  // Once you have your heading, you must then add your 'Declination Angle', which is the 'Error' of the magnetic field in your location.
  // Find yours here: http://www.magnetic-declination.com/
  // Mine is: -13* 2' W, which is ~13 Degrees, or (which we need) 0.22 radians
  // If you cannot find your Declination, comment out these two lines, your compass will be slightly off.
  float declinationAngle = -0.15155;//Pittsburgh!
  heading += declinationAngle;


////about 30 over the prioper place! robo club test march 27
  // Correct for when signs are reversed.
  if(heading < 0)
    heading += 2*PI;

  // Check for wrap due to addition of declination.
  if(heading > 2*PI)
    heading -= 2*PI;

  // Convert radians to degrees for readability.
  headingDegrees = heading * 180/M_PI; 
  }

}

void compassSetup(){
  if(debugSerial){
  Serial.print("compassSetup...");
  }
  Wire.begin(4);                // join i2c bus with address #4
  Wire.onReceive(receiveEvent); // register i2c event, call receiveEvent()
  
  //clean up your code by puttign the i2c stuff here
    /* Initialise the compass... */
  if(!mag.begin())
  {
    /* There was a problem detecting the HMC5883 ... check your connections */
    Serial.println("Ooops, no HMC5883 detected ... Check your wiring!");
    while(1);
  }
    if(debugSerial){
  Serial.println("complete!");
    }
}
