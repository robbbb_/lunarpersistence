#!/usr/bin/python

import time
from Adafruit_ADS1x15 import ADS1x15

gain = 4096  # +/- 4.096V
sps = 250  # 250 samples per second
ADS1115 = 0x01    # 16-bit ADC
adc = ADS1x15(ic=ADS1115)

# Read channel 0 in single-ended mode using the settings above
while(1):
    print "%.6f volts on pin %d" %( adc.readADCSingleEnded(0, gain, sps) / 1000,0)
    print "%.6f volts on pin %d" %( adc.readADCSingleEnded(1, gain, sps) / 1000,1)
    print "%.6f volts on pin %d" %( adc.readADCSingleEnded(2, gain, sps) / 1000,2)
    print "%.6f volts on pin %d" %( adc.readADCSingleEnded(3, gain, sps) / 1000,3)

    time.sleep(1)

# To read channel 3 in single-ended mode, +/- 1.024V, 860 sps use:
# volts = adc.readADCSingleEnded(3, 1024, 860)

