'''
Robb Godshaw - Motor Class for control - 
vague reference:
http://myrobotlab.net/tutorial-control-a-dc-motor-with-raspberry-pi/
--Safely and quietly spins a motor at a specified velocity.
This feels like a good application of oop. I wil have two motors, so the ability
simply have two instances seemed perfect. This file is currently for one motor
only, as I am awaiting a part to more consistently run my motors.

Inputs: Velocity desire
Outputs:a very large spinning object
'''

import time
import Adafruit_BBIO.PWM as PWM
import Adafruit_BBIO.GPIO as io

def almostEquals(x,y,epsilon):
    return(abs(y-x)<epsilon)

def mapp(input,minn, maxx, low, high):
    outSpan = abs(float(high) - float(low))
    inSpan =  abs(float(maxx) - float(minn))
    if inSpan == 0: return 0
    multiplicant = outSpan/inSpan
    output = int(multiplicant * input )
    return output

def mappTest():
    print 'testing your mapp function...',
    assert (mapp(33,0,99,0,9) == 3)
    assert (mapp(5,0,10,0,100) == 50)
    assert (mapp(5,0,10,10,0) == 5)
    assert (mapp(5,0,10,0,10) == 5)
    assert (mapp(5,0,0,0,0) == 0)
    assert (mapp(0,0,0,0,0) == 0)
    print 'passed!'

class motor(object):
    """L298n is a fantastic daul h bridge. This is good at driving it."""
    

    def __init__(self, panOrTilt):
        super(motor, self).__init__()
        self.velocity=1
        
        self.enableA_pin = "P9_14"
        self.in1_pin = "P9_15"  
        self.in2_pin = "P9_23"
        self.in3_pin = "P9_12"  
        self.in4_pin = "P9_27"
        self.enableB_pin = "P9_16"

        if panOrTilt == 'pan':
            print 'new pan motor'
            self.clockwisePin = self.in1_pin
            self.counterClockwisePin = self.in2_pin
            self.enablePin    = self.enableA_pin 
        elif panOrTilt == 'tilt':
            print 'new tilt motor'
            self.clockwisePin = self.in3_pin
            self.counterClockwisePin = self.in4_pin
            self.enablePin    = self.enableB_pin 


        io.setup(self.clockwisePin, io.OUT)
        io.setup(self.counterClockwisePin, io.OUT)
#        PWM.cleanup()
        io.cleanup()
#        print'clean'
#        self.motor_off()#safety


    
    
    def spinMotorAt(self,velocity):
        self.velocity=int(velocity)
        if self.velocity >99: self.velocity = 99
        if self.velocity <-99: self.velocity = -99
        if self.velocity==0 :
            self.motor_off()
        else:
            if self.velocity > 0 :
                self.clockwise()
            else:
                self.counter_clockwise()
            speed = abs(self.velocity)

            print "speed: %d" % speed
            mapSpeed = mapp(speed,0,99,0,99)
            print 'mapspeed: %d' % mapSpeed
            PWM.start(self.enablePin, mapSpeed, 2000, 0)
            

    def clockwise(self):
        ''' sets the direction pins for clockwise rotation'''
        io.output(self.clockwisePin, io.HIGH)   
        io.output(self.counterClockwisePin, io.LOW)

    def counter_clockwise(self):
        ''' sets the direction pins for counter_clockwise rotation'''
        io.output(self.clockwisePin, io.LOW)
        io.output(self.counterClockwisePin, io.HIGH)
        
    def motor_off(self):
        '''this ties the motor leads to eachother. preventing motion'''
        io.output(self.clockwisePin, io.LOW)
        io.output(self.counterClockwisePin, io.LOW)
        PWM.stop(self.enablePin)

    def motor_brake(self):
        '''this ties the motor leads to eachother. preventing motion'''
        io.output(self.clockwisePin, io.HIGH)
        io.output(self.counterClockwisePin, io.HIGH)
        PWM.stop(self.enablePin)

    def stop(self):
        self.motor_off()
        PWM.cleanup()


if (__name__ == "__main__"):
    '''This will allow the testing of motors'''
    motorType = raw_input("type 'pan' or 'tilt' to choose motor: ")
    testMotor=motor(motorType)
    try:
        while(1):
            testMotor.velocity = int(raw_input("Type speed -99 to 99:  "))
            testMotor.spinMotorAt(testMotor.velocity)
    except:
       print "EXCEPTION!"
       testMotor.stop()

