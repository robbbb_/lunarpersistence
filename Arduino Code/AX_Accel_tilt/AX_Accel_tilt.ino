//ADXL335




void setup(){
  Serial.begin(9600); 
}

void loop(){
  float accelVoltage = float(analogRead(A8));
  if(0){
    int minn = 1000;
    int maxx = 0;
    if(accelVoltage>maxx){
      maxx= accelVoltage;
    }
    if(accelVoltage<minn){
      minn= accelVoltage;
    }
    Serial.print ("minn= ");
    Serial.print (minn);
    Serial.print ("  maxx= ");
    Serial.println (maxx);
  }
  int minVal = 278;
  int maxVal = 412;
  int samplePin = A8;

  accelVoltage = constrain(accelVoltage,minVal,maxVal);
  float voltageSpan = abs(maxVal-minVal);
  float voltageOneUpOneDown = (((2.0*(accelVoltage-minVal))/voltageSpan))-1;
  //  float plusMinus = voltageOneUp
  Serial.print ("accelVoltage= ");
  Serial.println (accelVoltage);
  Serial.print ("  voltageOneUpOneDown= ");
  Serial.println (voltageOneUpOneDown);
  Serial.print ("span= ");
  Serial.print (voltageSpan);
  float result =  (acos(voltageOneUpOneDown) * 180.0 / PI)-90.0;
  Serial.print("Rotation:  ");
  Serial.println(result);
  delay(99);

}
