
void parseCoordString(){

  getPositionFromSerial();


}
void getPositionFromSerial(){
  //a blocking function that collects all the date from the UNIX board
  while (Serial.available()) {
    char inChar = Serial.read(); // receive byte as a character
    if (inChar == 'Q'){//if it is Q char, store it as final
      recievedBeacon = true;
      boolean rightLegnth = moonCoordStringCandidate.length() ==30;
      boolean rightBegin = moonCoordStringCandidate.charAt(0) == 'Z';
      boolean rightEnd = moonCoordStringCandidate.charAt(29) == '*';
      boolean rightEnd2 = moonCoordStringCandidate.endsWith("*");
      if (rightLegnth && rightBegin && rightEnd && rightEnd2){
        //        Serial.println("moonCoordStringCandidate passed all tests!");
        moonCoordString =moonCoordStringCandidate;
        moonCoordStringCandidate= "";//blank the old one

        azimuth100 = moonCoordString.substring(3,10).toInt();
        boolean azimuth100_OK = (36000>=azimuth100 && azimuth100>=0);
        //        Serial.print("azimuth100_OK?: ");
        //        Serial.println(azimuth100_OK);
        if(azimuth100_OK) azimuth = float(azimuth100)/100.00;

        altitude100 = moonCoordString.substring(13,20).toInt();
        boolean altitude100_OK = (9000>=altitude100&&altitude100>=-9000);
        //        Serial.print("altitude100_OK?: ");
        //        Serial.println(altitude100_OK);
        if(altitude100_OK) altitude = float(altitude100)/100.00;

        phase100 = moonCoordString.substring(23,30).toInt();
        boolean phase100_OK = (10000>=phase100&&phase100>=0);
        //        Serial.print("phase100_OK?: ");
        //        Serial.println(phase100_OK);

        if(phase100_OK) phase = float(phase100)/100.00;
        //        delay(200);
        debug();//the only call to debug, we know it is free to eat serial cuz it just finished sending some.
      }
      else{
        moonCoordStringCandidate= "";//blank the old one
      }

      //      Serial.print(rightLegnth);
      //      Serial.print(rightBegin);
      //      Serial.print(rightEnd);
      //      Serial.print(rightEnd2);
      //      Serial.println("moonCoordStringCandidate Failed a test!");

    }
    else{
      moonCoordStringCandidate += inChar ;
    }
  }
  //  if(recievedBeacon){

  //    Serial.print("moonCoordString :\t"); 
  //    Serial.print(moonCoordString);
  //    Serial.print("\t Azimuth :\t"); 
  //    Serial.print(azimuth);
  //    Serial.print("\tAltitude:\t"); 
  //    Serial.print(altitude);
  //    Serial.print("\tPhase:   \t"); 
  //    Serial.println(phase);

  //  }
  //  else{
  //   Serial.println("No position recieved from Linux Board.."); 
  //   delay(555);

  delay(55);
}


