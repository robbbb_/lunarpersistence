# -*- coding: utf-8 -*-
#UTF-8 allows for the ° symbol. It is a good symbol.

'''
Robb Godshaw - Main Loop - 100% homemade code.
all references are soley algotitmic and cited.
See adjacent files for library info

NOTE--THIS CODE WILL ONLY RUN ON A RASPBERRY PI!
'''
import time
import math #radian conversion and general trig
import sys #needed for pin control, done with files
import os #needed for pin control, done with files
import signal
from datetime import datetime, date
from Adafruit_LSM303DLHC import LSM303DLHC #Adafruit library for compass
import RPi.GPIO as io #module for GPIO use (digital in/out pins)

from motor import motor #I wrote this one! OOP for the win!! :-)
from moonObject import moon #I wrote this one too!
from robotObject import robot #I wrote this one too also! wow!


#I know these are globals, I ran out of time. I will oop later, I am sorry. 
Robot =robot()
Moon = moon(Robot.lat,Robot.lon)
panMotor = motor('pan')
tiltMotor = motor('tilt')

io.setmode(io.BCM)

stopButtonPin   = 24
startButtonPin  = 23
tiltLimitPin    = 25

#GPIO.setmode(GPIO.BOARD)
##Not suitable for BBB
io.setup(stopButtonPin , io.IN, pull_up_down=io.PUD_UP)
io.setup(startButtonPin, io.IN, pull_up_down=io.PUD_UP)
io.setup(tiltLimitPin ,  io.IN, pull_up_down=io.PUD_UP)

#def signal_handler(signal, frame):
#    #this is from adafruit. This is the only way I know to make the motors stop
#    #the very moment the program ends.
#        print '  You pressed Ctrl+C!'
#        emergencyStop()#stop everything, now
#        sys.exit(0)
#signal.signal(signal.SIGINT, signal_handler)

def emergencyStop():
    print '\nEMERGENCY STOP\n'
    try:panMotor.motor_brake()
    except:pass
    try:tiltMotor.motor_brake()
    except:pass
    sys.exit(0)

def buttonEvent(channel):
    print('event detected on %s'%channel),
    '''this event is called immediately when an event is detected.
    it runs in a seperate thread, so it is reliable.'''
    if channel == stopButtonPin:
        emergencyStop()
    elif channel == startButtonPin:
        print ' startButtonPin'
    elif channel == tiltLimitPin:
        print ' tiltLimitPin'
    else:
        print 'Something weird happened'

io.add_event_detect(stopButtonPin   , io.FALLING, callback=buttonEvent, bouncetime=200)  
io.add_event_detect(tiltLimitPin    , io.BOTH   , callback=buttonEvent, bouncetime=200)  



   

def PID_Control_Loop(desire, reality, KP,KI,KD):
    '''Written by Robb, referenced some psuedocode provided by Spencer Barton'''
    error = desire -reality
    print "Desire %.1f Reality %.1f Error %.1f"%(desire,reality,error),
    p = KP * error
    p = int(p)
    angularEpsilon = 5.0 #5° is acceptable at this stage
    fullCirlce = 360
    print 'output: ', p
    if not almostEquals(desire,reality,angularEpsilon):
        if abs(error)>fullCirlce/2: return -p
        else: return p
    else: return 0 
    
def panToAzimuthPID():
    (kP,Ki,Kd) = 0.3, 111,111
    directionOfMoon = Moon.azimuth
    directionOfRobot = Robot.heading
    desire = directionOfMoon
    reality = directionOfRobot
    output =PID_Control_Loop(desire,reality,kP,Ki,Kd)
    minSpeed, maxSpeed = 22,66
    
   # output = clamp(output, minSpeed, maxSpeed)
    panMotor.spinMotorAt(output)
    
    
def tiltToAltitudePID():
    (kP,Ki,Kd) = 0.1, 111,111
    directionOfMoon = Moon.altitude
    directionOfRobot = Robot.pitch
    desire = directionOfMoon
    reality = directionOfRobot
    output =PID_Control_Loop(desire,reality,kP,Ki,Kd)
    minSpeed, maxSpeed = 22,66
    
   # output = clamp(output, minSpeed, maxSpeed)
    tiltMotor.spinMotorAt(output)


def clamp(input, maxx, minn):
    magnitude = abs(input)
    # sign = 
    if magnitude>maxx: return maxx
    elif magnitude < minn: return minn
    else: return input
def maddyMap(input,minn,maxx,low,high):
    #maddy varner(mvarner) helped with this code
    low,high = min(low,high),max(low,high)
    minn,maxx = min(minn,maxx),max(minn,maxx)
    outSpan = (maxx - minn)
    if outSpan ==0 :return 0
    return (((input - minn) * (high - low)) / (outSpan)) - low


def almostEquals(x,y,epsilon):
    return(abs(y-x)<=epsilon)


def loop():
    isHellFrozenOver = False
    while not isHellFrozenOver: #forever, I think
        delay = 0.4 #time in seconds to rest between cycles
        time.sleep(delay)
        print 'looping'
        panToAzimuthPID()#aspire to proper azimuth
        tiltToAltitudePID()
        Moon.update()
        Robot.update()

io.wait_for_edge(startButtonPin, io.RISING)

try:
    loop()
except:
    print 'caught exeption: %s' % sys.exc_info()[0]
    for i in sys.exc_info():
        print i
    emergencyStop()