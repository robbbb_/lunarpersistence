/*
This section of code deals with the manner in which the pitch of the robot is determined.
 Pitch == altitude == tilt
 
 You have connected a 100nF capacitor btwn pitch pot wiper and GND
 this eliminates weird noise.
 On the black cord from the pot, you have connected:
 Orange to GND
 Yellow(wiper) to A8
 Black to VCC 5v
 
 LIMIT SWITCHES NOT YET IMPLEMENTED
 */
int pitchPin = A8; //analog 8 pot for pitch encoder
    int minn = 1000;
    int maxx = 0;
int prevAspeed = 0;
//int prevBspeed = 0;
int pitchHigh = 54;//mar23 2014- these are fine.
int pitchLow = 760;
int pitchMid = 411;
int wiggleRoom = 50;
int dangerHigh = pitchHigh + wiggleRoom;
int dangerLow = pitchLow - wiggleRoom;

void getPitch(){


   const int minVal = 271;
  const int maxVal = 410;
   const float voltageSpan = abs(maxVal-minVal);
  int samplePin = pitchPin;
  float accelVoltage = float(analogRead(samplePin));
  bool calibrateAccel = true;
  if(calibrateAccel){

    if(accelVoltage>maxx){
      maxx= accelVoltage;
    }
    if(accelVoltage<minn){
      minn= accelVoltage;
    }
    Serial.print ("minn= ");
    Serial.print (minn);
    Serial.print ("\tmaxx= ");
    Serial.println (maxx);
  }


  accelVoltage = constrain(accelVoltage,minVal,maxVal);

  float voltageOneUpOneDown = (((2.0*(accelVoltage-minVal))/voltageSpan))-1;
  // Serial.print ("accelVoltage= ");
  // Serial.println (accelVoltage);
  // Serial.print ("  voltageOneUpOneDown= ");
  // Serial.println (voltageOneUpOneDown);
  // Serial.print ("span= ");
  // Serial.print (voltageSpan);
  float tiltFromAccel =  (acos(voltageOneUpOneDown) * 180.0 / PI)-90.0;
  Serial.print("tiltFromAccel:  ");
  Serial.println(tiltFromAccel);
  // delay(99);
  int quickfix=map(int(tiltFromAccel),-70,70,-90,70);
  
  int pitchReadingSpan = abs(pitchHigh-pitchLow);
  int maxPitch = 90;//degreees
  int minPitch =-90;//degrees

  currentPitch= 1.0*quickfix;


}
