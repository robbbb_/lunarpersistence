/* Lunar Persistance Apparatus
 ~Main Tab~
 
 the teensy(arduino) is conencted to a BeagleBone black by i2c link address: 0x04
 the linux board is constantly calculatting the current apparent lunar
 position from Pittsburgh.
 //linux pwd is temppwd
 -BoneCommunication.ino tab handles i2c and data parsing.
 -joystick.ino tab is not implemented, but it is for manual control
 -phase.ino tab is not implemented, but it does phase. It will be trivial.
 -pitch.ino deals with the encoder on the pitch/tilt/altitude axis
 -Compass.ino deals with compass setup and parsing
 -spinAt.ino makes an oop motor object for spinning the two motors
 inludes safety checks and braking
 
 */

#include <Adafruit_Sensor.h>
#include <Adafruit_HMC5883_U.h>
#include <Wire.h>
#include <Servo.h> 
#include <PID_v1.h>

Adafruit_HMC5883_Unified mag = Adafruit_HMC5883_Unified(12345); //init the compass

double altSetpoint, altInput, altOutput; //PID variables, volitile
double aziSetpoint, aziInput, aziOutput;

//Specify the links and initial tuning parameters
//        reality      speed255      desire     P  I D direction
PID altPID(&altInput, &altOutput, &altSetpoint,40,0,0, DIRECT);
PID aziPID(&aziInput, &aziOutput, &aziSetpoint,3,0,0, REVERSE);

//These store the raw lunar coordinates from the linux board
String moonCoordString;//complete string
String moonCoordStringCandidate;//partial string
boolean recievedBeacon= false;
//These store the parsed true lunar coordinates
float azimuth;
float altitude;
float phase;
long azimuth100;//these are in hundreths of degrees for easy transmission as long integers
long altitude100;
long phase100;

//positions of robot in reality:
float headingDegrees;//Azimuth/pan/heading in degrees
int currentPitch;//tilt/altitude/pitch in degrees
int pitchEncoder;//out of 1023 this is global to allow for safety checks in SpinAt

void setup()
{
  debugSetup();//start serial, print header
  motorSetup();//initialize all the motor stuff in SpinAt
  compassSetup();//see compass tab
  PIDsetup();//used in the PID library
}


const int numberPotPin = A0;  // Analog input pin that the potentiometer is attached to

int numberPotValue = 0;        // value read from the pot



void getNumberPot() {
  numberPotValue = analogRead(numberPotPin);            
  
  Serial.print("number = " );                       
  Serial.println(map(numberPotValue, 0, 1023, 0, 1001)); 
  delay(2);     //await adc settle                
}

void loop(){
  if(recievedBeacon){
   debug();//print table of all needed data-- comment out and test for hiesenbugs-- or tie to switch
   parseCoordString();//get true position of the moon from linux SBC
  }
  else{
   Serial.println("No position recieved from Linux Board.."); 
  }
  
  delay(100);//likely neeedeed
  getHeading();//fetch true heading from compass
  getPitch();//get true pitch from mapped potentiometer

  PIDpollinputs();//tell the pid algorithm its parameters
  altPID.Compute();//calculate outputs
  aziPID.Compute();//calculate outputs
  spinAt(1,altOutput);//spin motors to speed determined by PID
  spinAt(0,aziOutput);
  safetyCheck();
}

void PIDsetup()
{
  //initialize the variables we're linked to
  altPID.SetMode(AUTOMATIC);
  altPID.SetOutputLimits(-255, 255);
  aziPID.SetMode(AUTOMATIC);
  aziPID.SetOutputLimits(-255, 255);
  PIDpollinputs();//Need?
}

void PIDpollinputs(){
  //For Azimuth.heading/pan, input is circular. 1 degree is 2 degrees from 359 degrees.
  //the conditionals ensure smoth motion across the wraparound
  aziInput = headingDegrees;
  aziSetpoint = azimuth;
  int aziError = aziSetpoint - aziInput;
  if     (aziError >180)   aziInput+=360;
  else if(aziError<-180)   aziInput-=360;
  aziError = aziSetpoint - aziInput;
  if (-180 > aziError | aziError > 180){
  Serial.println("aziError is unreasonable. Wraparound not properly implemented.");
  }
  
 //Altitude/tilt/pitch, input is ordinary
  altInput = (currentPitch);
  altSetpoint = (altitude);
}

void debugSetup(){
  Serial.begin(9600);// start serial for debug output
  Serial.println("Welcome to the Lunar Persistance Apparatus"); 
  Serial.println("\t An immersive and cosmic mechatronic sculpture");
  Serial.println("");
  Serial.println("");
}
  
void debug(){
  ///All print statements go here
//  if (Serial.available() > 0) {
Serial.print("~~ Data! ~~\t"); Serial.print("Moon  \t"); Serial.print("Robot \t"); Serial.println("Output\t");//Serial.print("ErRor \t");
Serial.print("Azimuth :\t"); Serial.print(azimuth);Serial.print("\t"); Serial.print(headingDegrees);Serial.print("\t"); Serial.println(aziOutput);
Serial.print("Altitude:\t"); Serial.print(altitude);Serial.print("\t"); Serial.print(currentPitch);Serial.print("\t"); Serial.println(altOutput);
Serial.print("Phase:   \t"); Serial.print(phase);Serial.print("\t"); Serial.print(phase);Serial.print("\t"); Serial.println("  __  ");//error look at
Serial.println();//new line....
Serial.print("moonCoordString:   \t");Serial.println(moonCoordString);
Serial.print("Pitch Encoder Volatge:   \t");Serial.println(pitchEncoder);
Serial.println();//new line....
//  }

}




