/* Lunar Persistance Apparatus

Main Tab

Calls all other tabs
///i2c reciever
///takes the position of the moon from the BeagleBone black 


*/

//uses i2c on port 0x04

#include <Adafruit_Sensor.h>
#include <Adafruit_HMC5883_U.h>
#include <Wire.h>
#include <Servo.h> 
#include <PID_v1.h>
Adafruit_HMC5883_Unified mag = Adafruit_HMC5883_Unified(12345);

double altSetpoint, altInput, altOutput;
double aziSetpoint, aziInput, aziOutput;


PID altPID(&altInput, &altOutput, &altSetpoint,10,0,0, DIRECT);
PID aziPID(&aziInput, &aziOutput, &aziSetpoint,10,0,0, REVERSE);

//These store the officila lunar coordinates from the linux board
String moonCoordString;
String moonCoordStringCandidate;
char charArray[32];//perhaps no longer in use
//once parsed:
float azimuth;
float altitude;
float phase;

//positions of robot in reality:
float headingDegrees;
int currentPitch;//in degrees
int pitchEncoder;//out of 1023 this is global to allow for safety checks in SpinAt

void setup()
{
  Wire.begin(4);                // join i2c bus with address #4
  Wire.onReceive(receiveEvent); // register event
  Serial.begin(9600);           // start serial for output
  motorSetup();//initialize all the motor stuff in SpinAt
  Serial.println("Welcome to the Lunar Persistance Apparatus"); 
  Serial.println("");
  PIDsetup();
  /* Initialise the compass... */
  if(!mag.begin())
  {
    /* There was a problem detecting the HMC5883 ... check your connections */
    Serial.println("Ooops, no HMC5883 detected ... Check your wiring!");
    while(1);
  }

}

void loop()
{
  delay(200);
  //  Serial.println(moonCoordString);
  getHeading();
  getPitch();
  parseCoordString();
  debug();
  //  Serial.print("What I am sending to pitchMotor)
  // spinAt(1,-222);
  //  int outputTilt=PID_Control_Loop(azimuth,headingDegrees,33.0);
PIDpollinputs();
  altPID.Compute();
  aziPID.Compute();
//  spinAt(1,altOutput);
//    spinAt(0,aziOutput);

  Serial.print( "altOutput: ");
  Serial.println (altOutput);

}


//Specify the links and initial tuning parameters

void PIDsetup()
{
  //initialize the variables we're linked to

  altPID.SetMode(AUTOMATIC);
  altPID.SetOutputLimits(-255, 255);


  aziPID.SetMode(AUTOMATIC);
  aziPID.SetOutputLimits(-255, 255);
  PIDpollinputs();
}
void PIDpollinputs(){
	  aziInput = int(headingDegrees);
  aziSetpoint = int(azimuth);
    altInput = int(currentPitch);
  altSetpoint = int(altitude);
}

void debug(){
  ///All print statements go here

 Serial.print("Azimuth: "); 
 Serial.println(azimuth);
  Serial.print("Altitude: "); 
  Serial.println(altitude);
 // Serial.print("Phase: ");	
 // Serial.println(phase);
 Serial.print("Heading (degrees): "); 
 Serial.println(headingDegrees);
  Serial.print("Pitch (degrees): "); 
  Serial.print(currentPitch);
  Serial.print("\tPitch (value): "); 
  Serial.println(pitchEncoder);

}




///the below is being abaondoned soon i think. I am using the library now.



int PID_Control_Loop(int desire, int reality, float KP){
  //Takes: goal. feedback, constant for proportional control
  // returns: output to run motor at

  //includes deadband for easyness. TO be removed later.

  int error = desire - reality;
  //Serial.print("Desire %.1f Reality %.1f Error %.1f"%(desire,reality,error));
  int p = KP * error; //KP is my constant tuned thing for proportional contol
  p = int(p);//no idea
  int angularEpsilon = 5; //5° is acceptable at this stage
  int fullCirlce = 360;
  //  Serial.print( "output: ");
  //  Serial.print (p);
  if (!almostEquals(desire,reality,angularEpsilon)){
    if (abs(error)>fullCirlce/2){
      return -p;
    }
    else{ 
      return p ;             
    }
  }
  else{
    return 0 ;
  }
}

boolean almostEquals(int x, int y,int epsilon){
  return(abs(y-x)<=epsilon);
}








