/*
This section of code deals with the manner in which the pitch of the robot is determined.
 Pitch == altitude == tilt
 
 You have connected a 100nF capacitor btwn pitch pot wiper and GND
 this eliminates weird noise.
 On the black cord from the pot, you have connected:
 Orange to GND
 Yellow(wiper) to A8
 Black to VCC 5v
 
 LIMIT SWITCHES NOT YET IMPLEMENTED
 */
int pitchPin = A8; //analog 8 pot for pitch encoder

int prevAspeed = 0;
//int prevBspeed = 0;
int pitchEncoder;//this is global to allow for safety checks in SpinAt
int pitchHigh = 924;//mar23 2014- these are fine.
int pitchLow = 496;


void getPitch(){

  int pitchReadingSpan = abs(pitchHigh-pitchLow);
  int maxPitch = 90;//degreees
  int minPitch =-90;//degrees

  pitchEncoder = analogRead(pitchPin);
  currentPitch= map (pitchEncoder,pitchLow,pitchHigh,minPitch,maxPitch);

}


