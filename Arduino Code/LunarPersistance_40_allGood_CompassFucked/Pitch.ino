
/*
This section of code deals with the manner in which the pitch of the robot is determined.
 Pitch == altitude == tilt
 
 You have connected a 100nF capacitor btwn pitch pot wiper and GND
 this eliminates weird noise.
 On the black cord from the pot, you have connected:
 Orange to GND
 Yellow(wiper) to A8
 Black to VCC 5v

 */
int pitchPin = A2; //analog 8 pot for pitch encoder/////////////////////////////////////
    int minn = 1000;
    int maxx = 0;
int prevAspeed = 0;
//int prevBspeed = 0;
int pitchHigh = 868;//AS INSTALLED.
int pitchLow = 215;//AS INSTALLED.
int pitchMid = 590;
int wiggleRoom = 50;
int dangerHigh = pitchHigh + wiggleRoom;
int dangerLow = pitchLow - wiggleRoom;

void getPitch(){






  // Serial.print ("accelVoltage= ");
  // Serial.println (accelVoltage);
  // Serial.print ("  voltageOneUpOneDown= ");
  // Serial.println (voltageOneUpOneDown);
  // Serial.print ("span= ");
  // Serial.print (voltageSpan);

  // delay(99);
 
  int pitchReadingSpan = abs(pitchHigh-pitchLow);
  int maxPitch = 90;//degreees
  int minPitch =-90;//degrees
pitchEncoder = analogRead(pitchPin);
  currentPitch= map(pitchEncoder,pitchLow,pitchHigh,-90,90);


}
