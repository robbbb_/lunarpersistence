///i2c reciever
///takes the position of the moon from the BeagleBone black 

//uses i2c on port 0x04

#include <Adafruit_Sensor.h>
#include <Adafruit_HMC5883_U.h>
#include <Wire.h>
#include <Servo.h> 

Adafruit_HMC5883_Unified mag = Adafruit_HMC5883_Unified(12345);


String moonCoordString;
String moonCoordStringCandidate;

//String azimuth100;//multiplied by 100 to transmit easier
//String altitude100; //UNIT: HECTO-DEGREES
//String phase100;

float azimuth;
float altitude;
float phase;
float headingDegrees;
int currentPitch;
char charArray[32];

void setup()
{
  Wire.begin(4);                // join i2c bus with address #4
  Wire.onReceive(receiveEvent); // register event
  Serial.begin(9600);           // start serial for output
  motorSetup();
  Serial.println("HMC5883 Magnetometer Test"); 
  Serial.println("");

  /* Initialise the sensor */
  if(!mag.begin())
  {
    /* There was a problem detecting the HMC5883 ... check your connections */
    Serial.println("Ooops, no HMC5883 detected ... Check your wiring!");
    while(1);
  }

}

void loop()
{
  delay(400);
  //  Serial.println(moonCoordString);
  getHeading();
  getPitch();
  parseCoordString();

  debug();
//  Serial.print("What I am sending to pitchMotor)
  //  spinAt(0,PID_Control_Loop(azimuth,headingDegrees,5.0));
//   spinAt(1,PID_Control_Loop(altitude,currentPitch,15.0));
}

int PID_Control_Loop(int desire, int reality, float KP){
  int error = desire - reality;
  //Serial.print("Desire %.1f Reality %.1f Error %.1f"%(desire,reality,error));
  int p = KP * error;
  p = int(p);
  int angularEpsilon = 5; //5° is acceptable at this stage
  int fullCirlce = 360;
  Serial.print( "output: ");
  Serial.print (p);
  if (!almostEquals(desire,reality,angularEpsilon)){
    if (abs(error)>fullCirlce/2){
      return -p;
    }
    else{ 
      return p ;             
    }
  }
  else{
    return 0 ;
  }
}

boolean almostEquals(int x, int y,int epsilon){
  return(abs(y-x)<=epsilon);
}





void debug(){
  ///All print statements go here

  Serial.print("Azimuth: "); 
  Serial.println(azimuth);
  Serial.print("Altitude: "); 
  Serial.println(altitude);
  Serial.print("Phase: ");	
  Serial.println(phase);
  Serial.print("Heading (degrees): "); 
  Serial.println(headingDegrees);
  Serial.print("Pitch (degrees): "); 
  Serial.println(currentPitch);

}




