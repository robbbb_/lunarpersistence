/*
SpinAt houses the function that spins the motors.
Pin assignments, safety checks, 

*/

//define all the control pins
//Motor A
int enableA = 14; //Enables for speed
int motorAIn1 = 16; //Ins for direction
int motorAIn2 = 17;

//Motor B
int motorBIn3 = 18;
int motorBIn4 = 19; 
int enableB = 15;
int pinA, pinB;
 
 


void motorSetup(){
  //initialize all the control pins as outputs
  pinMode(motorAIn1, OUTPUT);
  pinMode(motorAIn2, OUTPUT);
  pinMode(enableA, OUTPUT);
  pinMode(motorBIn3, OUTPUT);
  pinMode(motorBIn4, OUTPUT);
  pinMode(enableB, OUTPUT);
}

void spinAt( int whichMotor, int speed0255){ //Function to spin the motor at a speed

  int pinToPwm;
  int pinToLow;
  int pinToHigh;


  if (whichMotor==0){
    pinA = motorAIn1;
    pinB = motorAIn2;
    pinToPwm = enableA;

}
  else if (whichMotor==1){
    pinA = motorBIn3;
    pinB = motorBIn4;
    pinToPwm = enableB;
  }
  else{
    ///BROKEN
  }
  if (speed0255 ==0){
brake();
  }
  else if (speed0255 > 0){
clock();
  }
  else {
anticlock();
  }

  if (whichMotor==0){
    if(pitchEncoder>pitchHigh){
Serial.println("TOO HIGH");
//      speed0255 = 155;
clock();

  }
      if(pitchEncoder<pitchLow){
        Serial.println("TOO LOOO");
//      speed0255 = -155;
anticlock();
  }
}

  analogWrite(pinToPwm, abs(speed0255));
}

void clock(){
    digitalWrite(pinA, LOW);
  digitalWrite(pinB, HIGH);
}

void anticlock(){
    digitalWrite(pinA, HIGH);
  digitalWrite(pinB, LOW);
}

void brake(){
    digitalWrite(pinA, LOW);
  digitalWrite(pinB, LOW);
}

  


