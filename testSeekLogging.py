# handlertest.py
import logging
import logging.handlers
import speechHandler



# Create a logging object (after configuring logging)
logging.basicConfig(filename='Beacon_Error_log.out',level=logging.DEBUG)
logger = logging.getLogger()

# A little trickery because, at least for me, directly creating
# an TALKHandler object didn't work
logging.handlers.TALKHandler = speechHandler.TALKHandler

# create the handler object
testHandler = logging.handlers.TALKHandler()
# Configure the handler to only send SMS for critical errors
testHandler.setLevel(logging.INFO)

# and finally we add the handler to the logging object
logger.addHandler(testHandler)

# And finally a test
logger.debug('Test 1')
logger.info('Test 2')
logger.warning('Test 3')
logger.error('Test 4')
logger.critical('Test 5')