import serial
from moonObject import moon #I wrote this one!
import time
import logging
import ephem #only for time travel
#mac only. untested.
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

fh = logging.FileHandler('Beacon_Error_log.out')
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
logger.addHandler(fh)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(formatter)
logger.addHandler(ch)

lon, lat = '-79.9449','40.4425' #from google maps, CMU
Moon = moon(lat,lon,ephem.now())

serialConnected = False
usbport = '/dev/tty.usbserial-A9CNNLDD' #OSX EXAMPLE
# usbport = '/dev/tty.usbserial-A9CNNLDD64' #OSX EXAMPLE
# usbport = '/dev/ttyUSB0' #rpi
baud = 9600
#pyserialAdvice: http://stackoverflow.com/questions/21073086/wait-on-arduino-auto-reset-using-pyserial
while not serialConnected:
    try:
        logger.info("Attempting to initialize Serial on port %s with baud %d"%(usbport,baud))
        arduino = serial.Serial(usbport,
                 baudrate=baud,
                 bytesize=serial.EIGHTBITS,
                 parity=serial.PARITY_NONE,
                 stopbits=serial.STOPBITS_ONE,
                 timeout=2,#i changed from 1
                 xonxoff=0,
                 rtscts=0
                 )
# Toggle DTR to reset Arduino
        arduino.setDTR(False)
        time.sleep(1)
        # toss any data already received, see
        # http://pyserial.sourceforge.net/pyserial_api.html#serial.Serial.flushInput
        arduino.flushInput()
        arduino.setDTR(True)
        serialConnected = True
        logger.info("Serial Sucsessful!"),
    except Exception, e:
        logger.critical("Serial Initialization Error %s" % e)
        time.sleep(3)



def checkTime():
    ephemNowThen = 41744.0 #ephemDate float of late april 2014
    if(ephemNowThen > float(ephem.now())):
        # time is wrong, we are in the past
        logger.critical("System time is slow. RTC Error.")
        date =ephem.now()
        logging.info(date)
    else:
        logger.info("Sucsessfully checked time. It is after yesterday")

def updateAndSendNumbers():
    '''Updates all the coordinates of the moon. All floats.'''
    Moon.update(ephem.now())
    multiplier = 100
    azi100 = Moon.azimuth*multiplier
    alt100 = Moon.altitude*multiplier
    phz100 = Moon.robbPhase*multiplier
#test
    # (azi100,alt100,phz100)=(0,9000,10100)#MAXIMUM TILT
    # (azi100,alt100,phz100)=(0,-9000,10100)#MINUMU TILT
    # (azi100,alt100,phz100)=(-100,-9100,-100)#bounds test
    # (azi100,alt100,phz100)=(36100,9100,10100)#bounds test
    comString = 'Zz=%+06d*Al=%+06d*Ph=%+06d*Q' % (azi100,alt100,phz100)

    if(len(comString) == 31):
        try:
            logger.debug("Sending String: %s to Arduino over serial bus"% comString)
            arduino.write(comString)
        except Exception, e:
            logger.critical("Error sending comString to Arduino: %s" % e)
            time.sleep(2)
    # return comString

def main():

    logger.info('')
    logger.info('Welcome')
    logger.info('To the Lunar Persistance Apparatus')
    logger.info('     Debug Log     :-)')
    logger.info('')
    logger.info('Current Robot Coordinates areLattitude: %s Longitude: %s' % (lat,lon))
    logger.info('')

    checkTime()
    # initialComString = updateAndSendNumbers()
    # logger.info("Sending first String: %s to Arduino over serial bus"% initialComString)
    with arduino:
        while True:
            updateAndSendNumbers()
            time.sleep(0.200)
            try:

                #attempt to relay arduino serial to log
                readline = 'NadA'
                i=0
                while readline:
                    readline = arduino.readline()
                    readline = readline.replace('\n','')#strip returns
                    readline = readline.replace('\r','')
                    logger.info('Arduino %d:: %s' % (i,readline))
                    i+=1


            except Exception, e:
                logger.critical("Error printing Arduino outstream: %s" % e)

            secondsToWaitBetweenPackets = 0.999
            # time.sleep(secondsToWaitBetweenPackets)
if __name__ == "__main__":
    main()