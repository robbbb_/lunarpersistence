# -*- coding: utf-8 -*-
#UTF-8 allows for the ° symbol. It is a good symbol.

'''
Robb Godshaw - Main Loop - 100% homemade code.
all references are soley algotitmic and cited.
See adjacent files for library info

NOTE--THIS CODE WILL ONLY RUN ON A RASPBERRY PI!
'''
import time
import math #radian conversion and general trig
import sys #needed for pin control, done with files
import os #needed for pin control, done with files

from datetime import datetime, date
from Adafruit_LSM303DLHC import LSM303DLHC #Adafruit library for compass
import RPi.GPIO as io #module for GPIO use (digital in/out pins)
from Adafruit_ADS1x15 import ADS1x15 #adafruid library for analog to dig chip

from motor import motor #I wrote this one! OOP for the win!! :-)
from moonObject import moon #I wrote this one too!
from robotObject import robot #I wrote this one too also! wow!

class joyControl(object):
    """docstring for joyControl"""
    def __init__(self):
        self.Robot =robot()
#        self.Moon = moon(Robot.lat,Robot.lon)
        self.panMotor = motor('pan')
        self.tiltMotor = motor('tilt')

        io.setmode(io.BCM)
        #GPIO.setmode(GPIO.BOARD)

        self.stopButtonPin   = 24
        self.startButtonPin  = 23
        self.tiltLimitPin    = 25

        io.setup(self.stopButtonPin , io.IN, pull_up_down=io.PUD_UP)
        io.setup(self.startButtonPin, io.IN, pull_up_down=io.PUD_UP)
        io.setup(self.tiltLimitPin ,  io.IN, pull_up_down=io.PUD_UP)





        
    def panToAzimuthPID(self, ):
        (kP,Ki,Kd) = 0.3, 111,111
        directionOfMoon = self.headingGoal
        directionOfRobot = Robot.heading
        desire = directionOfMoon
        reality = directionOfRobot
        output =PID_Control_Loop(desire,reality,kP,Ki,Kd)
        minSpeed, maxSpeed = 22,66
        
       # output = clamp(output, minSpeed, maxSpeed)
        panMotor.spinMotorAt(output)
        
        
    def tiltToAltitudePID(self, ):
        (kP,Ki,Kd) = 0.1, 111,111
        directionOfMoon = Moon.altitude
        directionOfRobot = Robot.pitch
        desire = directionOfMoon
        reality = directionOfRobot
        output =PID_Control_Loop(desire,reality,kP,Ki,Kd)
        minSpeed, maxSpeed = 22,66
        
       # output = clamp(output, minSpeed, maxSpeed)
        tiltMotor.spinMotorAt(output)
    def getPitchTarget(self):

        self.pitchTarget= None

    def getJoyVolts(self):
        gain = 4096  # +/- 4.096V
        sps = 250  # 250 samples per second
        ADS1115 = 0x01    # 16-bit ADC
        adc = ADS1x15(ic=ADS1115)
        pitchEncoderPin = 2
        headingEncoderPin = 3
        self.azimuthJoyVolts= adc.readADCSingleEnded(pitchEncoderPin, gain, sps)
        self.altitudeJoyVolts = adc.readADCSingleEnded(headingEncoderPin, gain, sps)
        self.azimuthJoyMag = self.getJoyMagnitudes(self.azimuthJoyVolts)
        self.altitudeJoyMag = self.getJoyMagnitudes(self.altitudeJoyVolts)


    def getJoyMagnitudes(self,signal):
        center = 1666
        maxx=3333
        deadBandWidth = 444
        lowDeadBound = center - deadBandWidth
        hiDeadBound = center + deadBandWidth
        dead=(signal > lowDeadBound and signal < hiDeadBound)
        if dead:
            return 0
        return map(signal,0,maxx,-99,99)

def emergencyStop():
    print '\nEMERGENCY STOP\n'
    try:self.panMotor.motor_brake()
    except:pass
    try:self.tiltMotor.motor_brake()
    except:pass
    io.cleanup()
    sys.exit(0)

def buttonEvent(channel):
    print('event detected on %s'%channel),
    '''this event is called immediately when an event is detected.
    it runs in a seperate thread, so it is reliable.'''
    if channel == self.topButtonPin:
        emergencyStop()
    elif channel == self.startButtonPin:
        print ' startButtonPin'
    elif channel == self.tiltLimitPin:
        print ' tiltLimitPin'
    else:
        print 'Something weird happened'

#io.add_event_detect(stopButtonPin   , io.FALLING, callback=buttonEvent, bouncetime=200)  
#io.add_event_detect(tiltLimitPin    , io.BOTH   , callback=buttonEvent, bouncetime=200)  


def clamp(inputt, minn, maxx):
    if inputt>maxx: return maxx
    elif inputt < minn: return minn
    else: return inputt


def testClamp():
    print ' testing clamp...',
    assert(clamp(555,-222,222)==222)
    assert(clamp(0,-222,222)==0)
    assert(clamp(-555,-222,222)== -222)
    assert(clamp(555,-222,0)==0)
    assert(clamp(-555,0,222)==0)
    print 'passed!'

def map( x,  in_min,  in_max,  out_min,  out_max):
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;


def maddyMap(inputt,minn,maxx,low,high):
    #maddy varner(mvarner) helped with this code
    low,high = min(low,high),max(low,high)
    minn,maxx = min(minn,maxx),max(minn,maxx)
    outSpan = (maxx - minn)
    if outSpan ==0 :return 0
    return (((inputt - minn) * (high - low)) / (outSpan)) - low

def PID_Control_Loop(desire, reality, KP,KI,KD):
    '''Written by Robb, referenced some psuedocode provided by Spencer Barton'''
    error = desire -reality
    print "Desire %.1f Reality %.1f Error %.1f"%(desire,reality,error),
    p = KP * error
    p = int(p)
    angularEpsilon = 5.0 #5° is acceptable at this stage
    fullCirlce = 360
    print 'output: ', p
    if not almostEquals(desire,reality,angularEpsilon):
        if abs(error)>fullCirlce/2: return -p
        else: return p
    else: return 0 

def almostEquals(x,y,epsilon):
    return(abs(y-x)<=epsilon)

def joyRunner():
    joy = joyControl()
    isHellFrozenOver = False
    while not isHellFrozenOver: #forever, I think
        delay = 0.2 #time in seconds to rest between cycles
        time.sleep(delay)
        joy.getJoyVolts()
        print 'x: %d y: %d'% (joy.azimuthJoyMag, joy.altitudeJoyMag)
        joy.panMotor.spinMotorAt(joy.azimuthJoyMag)
        joy.tiltMotor.spinMotorAt(joy.altitudeJoyMag)

def loop():
    isHellFrozenOver = False
    while not isHellFrozenOver: #forever, I think
        delay = 0.4 #time in seconds to rest between cycles
        time.sleep(delay)
        print 'looping'
        joyRunner()
        # panToAzimuthPID()#aspire to proper azimuth
        # tiltToAltitudePID()
        # Moon.update()
        # Robot.update()

#io.wait_for_edge(23, io.RISING)

try:
    joyRunner()
    loop()
except:
    print 'caught exeption: %s' % sys.exc_info()[0]
    for i in sys.exc_info():
        print i
    emergencyStop()