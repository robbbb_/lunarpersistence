/* far from complete servo driving code


*/


Servo phaseServo;  // create servo object to control a servo 
 

void phaseSetup() 
{ 
  phaseServo.attach(9);  // attaches the servo on pin 9 to the servo object 
} 
 
void changePhase() 
{ 
  val = analogRead(potpin);            // reads the value of the potentiometer (value between 0 and 1023) 
  val = map(val, 0, 1023, 0, 179);     // scale it to use it with the servo (value between 0 and 180) 
  phaseServo.write(val);                  // sets the servo position according to the scaled value 
} 
