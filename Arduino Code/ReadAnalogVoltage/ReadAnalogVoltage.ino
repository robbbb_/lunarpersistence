/*
  ReadAnalogVoltage
 Reads an analog input on pin 0, converts it to voltage, and prints the result to the serial monitor.
 Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.
 
 This example code is in the public domain.
 */

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
}

// the loop routine runs over and over again forever:
void loop() {
//  getNumberPot();
  getPitch();
  //  Serial.println(sensorValue);
    delay(100);
}


const int numberPotPin = A1;  // Analog input pin that the potentiometer is attached to
int numberPotValue = 0;        // value read from the pot

void getNumberPot() {
  numberPotValue = analogRead(numberPotPin);            
  Serial.print("numberPotValue = " ); 
  Serial.print(numberPotValue); 
  Serial.print("number = " );                       
  Serial.println(map(numberPotValue, 0, 1023, 0, 999)); 
  delay(77);     //await adc settle                
}






int pitchPin = A2; //analog 8 pot for pitch encoder/////////////////////////////////////
int minn = 0;
int maxx = 1000;
int pitchHigh =1000;//mar23 2014- these are fine.
int pitchLow = 0;
int pitchMid = 411;
int wiggleRoom = 50;
int dangerHigh = pitchHigh + wiggleRoom;
int dangerLow = pitchLow - wiggleRoom;

void getPitch(){






  // Serial.print ("accelVoltage= ");
  // Serial.println (accelVoltage);
  // Serial.print ("  voltageOneUpOneDown= ");
  // Serial.println (voltageOneUpOneDown);
  // Serial.print ("span= ");
  // Serial.print (voltageSpan);

  // delay(99);

  int pitchReadingSpan = abs(pitchHigh-pitchLow);
  int maxPitch = 90;//degreees
  int minPitch =-90;//degrees
  int pitchEncoder = analogRead(pitchPin);
  delay(22);
  int currentPitch= map(pitchEncoder,pitchLow,pitchHigh,minPitch,maxPitch);
 
  Serial.print("pitchEncoder  =\t" ); 
  Serial.print(pitchEncoder); 
  Serial.print("\tcurrentPitch =\t" );                       
  Serial.println(currentPitch); 
  


}

