/*
SpinAt houses the function that spins the motors.
 Pin assignments, safety checks, 
 
 */


Servo panSaberServo;
Servo tiltSaberServo;

//define all the control pins



void motorSetup(){
  //initialize all the control pins as outputs
//  Serial.println("Motor Control Initialized.");
  panSaberServo.attach(9);  // attaches the servo on pin 9 to the servo object 
  tiltSaberServo.attach(7);  // attaches the servo on pin 9 to the servo object 

}

void spinAt( int whichMotor, int speed0255){ //Function to spin the motor at a speed


  if (speed0255 > 255){
    speed0255 = 255;
  }

  if (speed0255 < -255){
    speed0255 = -255;
  }
  //0 is azi, pan
  //  Serial.print( "Attempting to spin "); 
  //if (whichMotor == 0){
  //  Serial.print ("pan motor");
  //}
  //else{
  //    Serial.print ("tilt motor");
  //}
  //  Serial.print( " at a speed of"); Serial.println (speed0255);
  int servoSignal = map(speed0255,-255,255, 1000,2000);//min 1000 neu 1500 max 2000
  if (whichMotor==0){//Pan
    panSaberServo.writeMicroseconds(servoSignal);

  }
  else if (whichMotor==1){
    tiltSaberServo.writeMicroseconds(servoSignal);
  }
  else{
    ///BROKEN
  }
  // if (speed0255 ==0){
  //   brake();
  // }
  // else if (speed0255 > 0){
  //   clock();
  // }
  // else {
  //   anticlock();
  // }


}

void safetyCheck(){

  int maxPitch = 88;//degreees
  int minPitch =-88;//degrees


  int highSwitchPin = 0;
  int lowSwitchPin = 1;

  if (false){
    //  if (whichMotor==1){
    //  if(currentPitch>maxPitch){
    while (digitalRead(highSwitchPin)==HIGH){
      Serial.println("TOO HIGH");
      //      speed0255 = 155;
      spinAt(1,-255);


    }
    //  if(currentPitch<minPitch){
    while (digitalRead(lowSwitchPin)==HIGH){
      Serial.println("TOO LOOO");
      //      speed0255 = -155;
      //      clock();
      spinAt(1,255);
    }
  }
}





