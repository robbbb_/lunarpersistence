int pin = 11;
volatile int state = LOW;

void setup()
{
  Serial.begin(9600);
  static int pitchLimitSwitchInterruptPin = 3;
  static int eStopInterruptPin = 2;
  pinMode(pin, OUTPUT);
  attachInterrupt(eStopInterruptPin, eStop, CHANGE);
  attachInterrupt(pitchLimitSwitchInterruptPin, blink, CHANGE);
}

void loop()
{
  digitalWrite(pin, state);
}

void blink()
{
  state = !state;
}
void eStop(){
  Serial.print("Oh no!");
}
