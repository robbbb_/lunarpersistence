# -*- coding: utf-8 -*-
#UTF-8 allows for the ° symbol. It is a good symbol.

'''
Robb Godshaw - Moon Object- 100% homemade code.
all references are soley algotitmic and cited.

THE MOON Object
given a time and a place, the moon object holds all the traits of our nearest
celestial neighbor. 

Advantages of using a moon object include clean code, extensibility, an easy Way
to generate moon-based graphics in the hypothetical future, and the ability to 
develop without being connected to the Raspberry Pi.

'''
import time
import ephem # astronomical data, moon position and phase
import math #radian conversion and general trig

class moon(object):
    """the moon object holds all of the significant properties of the moon
    at a specified place and time.
    """
    def __init__(self, lattitude, longitude, dateTime=0):
        super(moon, self).__init__()
        self.lattitude = lattitude
        self.longitude = longitude
        self.update()
        if dateTime == 0:
#            print 'defaulting'
            self.dateTime = ephem.now()
        else:
#            print 'not defaulting' 
            self.dateTime = ephem.date(dateTime)
            # print dateTime 
        
    def update(self,dateTime=0):
        if dateTime == 0:
#           print 'defaulting'
            self.dateTime = ephem.now()
        else:
#            print 'not defaulting' 
            self.dateTime = dateTime
            # print dateTime 

        self.azimuthAltitudeCalc()
        self.phaseCalc()
        
    def __str__(self):
        return \
        'Current lunar Heading is %s (%.1f° clockwise from magnetic north)'% \
        (headingToCardinal(self.azimuth),self.azimuth) + \
         '\nCurrent lunar Pitch is %s(%.1f° %s horizon)' %\
        (pitchToVernacular(self.altitude),self.altitude,\
            ('above' if self.altitude>0 else 'below')) +\
         '\nCurrent mechPhase is %.2f%% ' % (self.robbPhase)+\
         '\nCurrent truePhase is %.2f%% ' % (self.phase)

    def azimuthAltitudeCalc(self):
        self.moonObject = ephem.Moon()
        whereAndWhen = ephem.Observer()
        whereAndWhen.date = str(self.dateTime)
        whereAndWhen.lon, whereAndWhen.lat = self.longitude, self.lattitude

        self.moonObject.compute(whereAndWhen)
        self.now = whereAndWhen.date
        self.phase = self.moonObject.phase
        self.altitude = math.degrees((self.moonObject.alt))
        self.azimuth = math.degrees((self.moonObject.az))

    def phaseCalc(self, timee=0):
        ''' returns %% illuminated of moon at time'''
        #a version of phase that relates to 360° rotation of my phase mechanism
        if timee == 0: time=self.dateTime
        else: time = timee
        lastFull=ephem.previous_full_moon(time)
        nextFull=ephem.next_full_moon(time)
        period = nextFull- lastFull
        faralong = (time - lastFull) / period
        self.robbPhase = faralong*100
        if timee !=0: return self.robbPhase
    
    def testPhaseCalc(self):
        ''' determines if this truly cycles once per lunar month
        google says: 1 lunar month = 29.53059 days '''
        now = ephem.previous_full_moon(self.now)
        print 'the current float time is %f plus one is '% now, now+1
        print 'testing phaseCalc...',
        lunarMonth = 29
        for i in xrange(lunarMonth):
            print '%.2f %% is the magic number at day %d' % (self.phaseCalc(now+i),i)
        print 'if these numbers go from ~from 0% to 100%, you pass!'


def headingToCardinal(heading):
    fullCirlce=360.0
    cardinalQty =16
    wedgeSize = fullCirlce /cardinalQty
    offset = wedgeSize/2.0
    cardinalIndex = int(((heading+offset)%fullCirlce)/wedgeSize)
    cardinalList = \
    ['N','NNE','NE','ENE','E','ESE','SE','SSE',\
     'S','SSW','SW', 'WSW','W','WNW','NW','NNW']
    return cardinalList[cardinalIndex]

def pitchToVernacular(pitch):
    fullCirlce=360.0
    halfCircle = fullCirlce/2
    quarterCirle = halfCircle/2
    cardinalQty =16
    wedgeSize = fullCirlce /cardinalQty
    offset = wedgeSize/2.0
    angleIndex = int(((pitch+quarterCirle+offset)%fullCirlce)/wedgeSize)
    angleList = [
    'Directly underfoot',      #-90
    'Not quite directly underfoot',
    'Way below horizon',#-45°
    'below horizon',
    'At horizon', #------0°------
    'Above horizon',
    'High',#45°
    'Not quite Directly overhead',
    'Directly overhead',#90
    ]
    return angleList[angleIndex]

def almostEquals(x,y,epsilon):
    return(abs(y-x)<=epsilon)

def moonTest():
    ##this test needs testability
    longitude, lattitude = '-79.94493333333333','40.44251666666667'
    tenAM_ThanksgivingDay = 41605.145428
    navyAlt, navyAzi=32.8,223.1
    testMoon = moon(lattitude,longitude, tenAM_ThanksgivingDay)
#    print navyAzi, testMoon.azimuth, navyAlt, testMoon.altitude
    print 'testing your moon data against US Navy data from online....',
    assert almostEquals(testMoon.azimuth, navyAzi,3)
    assert almostEquals(testMoon.altitude, navyAlt,3)
    print 'Your data was within bounds!'
    print testMoon
    testMoon.testPhaseCalc()
    
def testPitchToVernacular():
    print 'testing pitchToVernacular...',
    assert pitchToVernacular(90) ==     'Directly overhead'
    assert pitchToVernacular(89) ==     'Directly overhead'
    assert pitchToVernacular(88) ==     'Directly overhead'
    assert pitchToVernacular(  60) ==     'Not quite Directly overhead'
    assert pitchToVernacular(  45) ==     'High'
    assert pitchToVernacular(  13) ==     'Above horizon'
    assert pitchToVernacular(  5) ==     'At horizon'
    assert pitchToVernacular(  0) ==     'At horizon'
    assert pitchToVernacular(  -5) ==     'At horizon'
    assert pitchToVernacular(  -15) ==     'below horizon'
    assert pitchToVernacular( -45) ==     'Way below horizon'
    assert pitchToVernacular( -70) ==     'Not quite directly underfoot'
    assert pitchToVernacular( -89) ==     'Directly underfoot'
    assert pitchToVernacular( -90) ==     'Directly underfoot'
    print 'Passed!'

def testHeadingToCardinal():
    print 'testing headingToCardinal...',
    assert headingToCardinal(0) == 'N'
    assert headingToCardinal(359) == 'N'
    assert headingToCardinal(358) == 'N'
    assert headingToCardinal(1) == 'N'
    assert headingToCardinal(3) == 'N'
    assert headingToCardinal(11) == 'N'
    assert headingToCardinal(13) == 'NNE'
    assert headingToCardinal(180) == 'S'
    assert headingToCardinal(190) == 'S'
    print 'Passed!'

def testAlmostEquals():
    print 'testing testAlmostEquals...',
    assert almostEquals(0,0,0.1) == True
    assert almostEquals(0,0,0) == True
    assert almostEquals(-1,-1,0) == True
    assert almostEquals(-0.1,-0.2,0.3) == True
    assert almostEquals(-0.1,-0.2,0.05) == False
    print 'Passed!'


def testAll():
    testHeadingToCardinal()
    testPitchToVernacular()
    testAlmostEquals()
#testAll()
#moonTest()
