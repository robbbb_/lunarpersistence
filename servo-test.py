'''
Robb Godshaw - Unedited example code
pasted form http://learn.adafruit.com/adafruits-raspberry-pi-lesson-8-using-a-servo-motor/software

allows for precise high torque rotational postioning

inputs: moonphase
outputs: rotation on the moon occlusion mechanism
'''

import time
def set(property, value):
    try:
        f = open("/sys/class/rpi-pwm/pwm0/" + property, 'w')
        f.write(value)
        f.close()   
    except:
        print("Error writing to: " + property + " value: " + value)
 
 
def setServo(angle):
    set("servo", str(angle))
    
        
set("delayed", "0")
set("mode", "servo")
set("servo_max", "180")
set("active", "1")
 
delay_period = 0.01
 
while True:
    for angle in range(0, 180):
        setServo(angle)
        time.sleep(delay_period)
    for angle in range(0, 180):
        setServo(180 - angle)
        time.sleep(delay_period)
        
        
#####BELOW LINE IS SCRAP CODE...

def setDuty(pin, duty):
    ''' drives pins in a manner more suitable for motors'''
    #duty is between 0 and 2047
    maxDuty = 2047.0
    maxTick = 4095
    minTick = 0
    dutyCoeff= maxDuty/maxTick
    onDuration= int(dutyCoeff*maxTick)
    offDuration = maxTick - onDuration
    tickToOn = offDuration/2
    tickToOff = tickToOn + onDuration
    pwm.setPWM(pin,tickToOn,tickToOff)

def spinAt(motor, speed):
    minDuty =0
    maxDuty = 2047
    if motor=='pan':
        pinToPulse,dirPinA,dirPinB=0,1,2
    elif motor == 'tilt':
        pinToPulse,dirPinA,dirPinB=5,4,3
    else: print "pan or tilt, no other values are valid"
    if speed < 0: #negative speed, spin backward
        setDuty(dirPinA,minDuty)
        setDuty(dirPinB,maxDuty)
    else:
        setDuty(dirPinA,maxDuty)
        setDuty(dirPinB,minDuty)
    #pulse enable to control speed
    setDuty(pinToPulse,speed)

def servoTarget(pin, target):
    pass
