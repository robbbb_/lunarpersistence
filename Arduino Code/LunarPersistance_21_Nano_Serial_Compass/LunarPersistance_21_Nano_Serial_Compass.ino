#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif

/* Lunar Persistance Apparatus
 ~Main Tab~
 
 the teensy(arduino) is conencted to a Rasp pi over serial
 the linux board is constantly calculatting the current apparent lunar
 position from Pittsburgh.
 //linux pwd is temppwd
 -BoneCommunication.ino tab handles i2c and data parsing.
 -joystick.ino tab is not implemented, but it is for manual control
 -phase.ino tab is not implemented, but it does phase. It will be trivial.
 -pitch.ino deals with the encoder on the pitch/tilt/altitude axis
 -Compass.ino deals with compass setup and parsing
 -spinAt.ino makes an oop motor object for spinning the two motors
 inludes safety checks and braking
 
 */


#include <Wire.h>
#include <Servo.h> 
#include <PID_v1.h>
#include <HMC5883L.h>

HMC5883L compass;

int compassErrorBoolean = 0;

double altSetpoint, altInput, altOutput; //PID variables, volitile
double aziSetpoint, aziInput, aziOutput;

//Specify the links and initial tuning parameters
//        reality      speed255      desire     P  I D direction
double altKp=20, altKi=0.00, altKd=0.06;
double aziKp=4.4, aziKi=0.00, aziKd=0.06;

PID altPID(&altInput, &altOutput, &altSetpoint,altKp,altKi,altKd, DIRECT);
PID aziPID(&aziInput, &aziOutput, &aziSetpoint,aziKp,aziKi,aziKd, DIRECT);

//These store the raw lunar coordinates from the linux board
String moonCoordString;//complete string
String moonCoordStringCandidate;//partial string
boolean recievedBeacon= false;
//These store the parsed true lunar coordinates
float azimuth= 180;
float altitude = 0;
float phase;
long azimuth100;//these are in hundreths of degrees for easy transmission as long integers
long altitude100;
long phase100;

//positions of robot in reality:
float headingDegrees;//Azimuth/pan/heading in degrees
int currentPitch;//tilt/altitude/pitch in degrees
int pitchEncoder;//out of 1023 this is global to allow for safety checks in SpinAt
const int highSwitchPin = 0;
const int lowSwitchPin = 1;
int samplePeriodMillis;

void setup()
{
  Serial.begin(9600);// start serial for debug output
  //debugSetup();//start serial, print header
  pinMode(highSwitchPin, INPUT_PULLUP);
  pinMode(lowSwitchPin, INPUT_PULLUP);
  //phaseSetup(); //later setup!
  motorSetup();//initialize all the motor stuff in SpinAt
  compassSetup();//see compass tab
  PIDsetup();//used in the PID library
  getsamplePeriodMillis();
}



void loop(){

  parseCoordString();//get true position of the moon from linux SBC
  delay(100);//likely neeedeed
  getHeading();//fetch true heading from compass
  getPitch();//get true pitch from mapped potentiometer
  PIDpollinputs();//tell the pid algorithm its parameters
  altPID.Compute();//calculate outputs
  aziPID.Compute();//calculate outputs
  spinAt(1,altOutput);//spin motors to speed determined by PID
  spinAt(0,aziOutput);
  safetyCheck();
  //  getNumberPot();
  //  aziKp = float(numberPotValue)/10.0;
  //  Serial.print("aziKp: ");
  //  Serial.println(aziKp);
  //  aziPID.SetTunings(aziKp, aziKi, aziKd);
  //  altKp = float(numberPotValue)/10.0;
  //  Serial.print("altKp: ");
  //  Serial.println(altKp);
  //  altPID.SetTunings(altKp, altKi, altKd);
}

void PIDsetup()
{
  //initialize the variables we're linked to
  altPID.SetMode(AUTOMATIC);
  altPID.SetOutputLimits(-255, 255);
  aziPID.SetMode(AUTOMATIC);
  aziPID.SetOutputLimits(-255, 255);
  PIDpollinputs();//Need?
}

void followMoon(){
  aziSetpoint = azimuth;
  altSetpoint = altitude;
}


void getsamplePeriodMillis(){
  //  /garbage
  int second =1000;
  samplePeriodMillis = 1;
}
void followNothing(){
  int second =1000;

  //  if (millis()%samplePeriodMillis> 
  aziSetpoint = random(0,259);
  altSetpoint = random(-89,89);
  delay(random(6*second,99*second));

}

void PIDpollinputs(){
  //For Azimuth.heading/pan, input is circular. 1 degree is 2 degrees from 359 degrees.
  //the conditionals ensure smoth motion across the wraparound
  aziInput = headingDegrees;



  int aziError = aziSetpoint - aziInput;
  if     (aziError >180)   aziInput+=360;
  else if(aziError<-180)   aziInput-=360;
  aziError = aziSetpoint - aziInput;
  if (-180 > aziError | aziError > 180){
    Serial.println("aziError is unreasonable. Wraparound not properly implemented.");
    //  
    followMoon();
    //void followNothing(){
  }

  //Altitude/tilt/pitch, input is ordinary
  altInput = currentPitch;
  }
  
  
void debug(){
  ///All print statements go here
//  if (Serial.available() > 0) {
Serial.print("~~ Data!~\t"); Serial.print("Moon  \t"); Serial.print("Robot \t"); Serial.println("Output\t");//Serial.print("ErRor \t");
Serial.print("Azimuth :\t"); Serial.print(azimuth);Serial.print("\t"); Serial.print(headingDegrees);Serial.print("\t"); Serial.println(aziOutput);
Serial.print("Altitude:\t"); Serial.print(altitude);Serial.print("\t"); Serial.print(currentPitch);Serial.print("\t"); Serial.println(altOutput);
Serial.print("Phase:   \t"); Serial.print(phase);Serial.print("\t"); Serial.print(phase);Serial.print("\t"); Serial.println("  __  ");//error look at
//Serial.println("EMPTY_!!");//new line....
Serial.print("moonCoordString:   \t");Serial.println(moonCoordString);
Serial.print("Pitch Encoder Volatge:   \t");Serial.println(pitchEncoder);
//Serial.println("CC");//new line....
//  }

}




//void debugSetup(){
//  Serial.begin(9600);// start serial for debug output
//  delay(100);
//  Serial.println("Welcome to the Lunar Persistance Apparatus"); 
//  Serial.println("\t An immersive and cosmic mechatronic sculpture");
//  Serial.println("AA");
//  Serial.println("BB");
//}
//  


const int numberPotPin = A1;  // Analog input pin that the potentiometer is attached to
int numberPotValue = 0;        // value read from the pot

void getNumberPot() {
  numberPotValue = analogRead(numberPotPin);            

  Serial.print("number = " );                       
  Serial.println(map(numberPotValue, 0, 1023, 0, 999)); 
  delay(2);     //await adc settle                
}





