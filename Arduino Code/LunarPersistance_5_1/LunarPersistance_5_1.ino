/* Lunar Persistance Apparatus
~Main Tab~

the teensy(arduino) is conencted to a BeagleBone black by i2c link address: 0x04
the linux board is constantly calculatting the current apparent lunar
position from Pittsburgh.

-BoneCommunication.ino tab handles i2c and data parsing.
-joystick.ino tab is not implemented, but it is for manual control
-phase.ino tab is not implemented, but it does phase. It will be trivial.
-pitch.ino deals with the encoder on the pitch/tilt/altitude axis
-Compass.ino deals with compass setup and parsing
-spinAt.ino makes an oop motor object for spinning the two motors
  inludes safety checks and braking

*/

#include <Adafruit_Sensor.h>
#include <Adafruit_HMC5883_U.h>
#include <Wire.h>
#include <Servo.h> 
#include <PID_v1.h>

Adafruit_HMC5883_Unified mag = Adafruit_HMC5883_Unified(12345); //init the compass

double altSetpoint, altInput, altOutput; //PID variables, volitile
double aziSetpoint, aziInput, aziOutput;
int myAziOutput;//global for easy printing

//Specify the links and initial tuning parameters
//////// reality      speed255      desire     p  I   D directio
PID altPID(&altInput, &altOutput, &altSetpoint,33,0,0, DIRECT);
// err = set - input ~
///if < - 180 ++ 360 from setpoint
///if > 180 -- 360 from setpoint
// ASSERT -180<error<180

//LIE about setoint

// call pid with aziSetpoint + 360
//PID aziPID(&aziInput, &aziOutput, &aziSetpoint,6,0,0, REVERSE);

//These store the raw lunar coordinates from the linux board
String moonCoordString;//complete string
String moonCoordStringCandidate;//partial string

//These store the parsed true lunar coordinates
float azimuth;
float altitude;
float phase;
  long azimuth100;//
  long altitude100;
  long phase100;


//positions of robot in reality:
float headingDegrees;//Azimuth/pan/heading in degrees
int currentPitch;//tilt/altitude/pitch in degrees
int pitchEncoder;//out of 1023 this is global to allow for safety checks in SpinAt

void setup()
{
Serial.begin(9600);// start serial for debug output
Serial.println("Welcome to the Lunar Persistance Apparatus"); 
Serial.println("\t An immersive and cosmic mechatronic sculpture");
motorSetup();//initialize all the motor stuff in SpinAt
compassSetup();//see compass tab
PIDsetup();//used in the PID library

  Wire.begin(4);                // join i2c bus with address #4
  Wire.onReceive(receiveEvent); // register i2c event, call receiveEvent()
  

}

void loop()
{
  delay(100);//likely neeedeed
  getHeading();
  getPitch();
  parseCoordString();
  debug();//print table of data
  /////LIE HEREE

  
  PIDpollinputs();
  altPID.Compute();
//  aziPID.Compute();
  spinAt(1,altOutput);
  // spinAt(0,aziOutput);//with PID library
  myAziOutput = PID_Control_Loop(aziSetpoint, aziInput, 0.55);
  spinAt(0, myAziOutput);
}

void PIDsetup()
{
  //initialize the variables we're linked to

  altPID.SetMode(AUTOMATIC);
  altPID.SetOutputLimits(-255, 255);

//  aziPID.SetMode(AUTOMATIC);
//  aziPID.SetOutputLimits(-255, 255);

  PIDpollinputs();
}

void PIDpollinputs(){
  aziInput = int(headingDegrees);
  aziSetpoint = int(azimuth);
  altInput = int(currentPitch);
  altSetpoint = int(altitude);
}

void debug(){
  ///All print statements go here
Serial.print("~~Data~~\t"); Serial.print("Moon  \t"); Serial.print("Robot \t"); Serial.println("Output\t");//Serial.print("ErRor \t");
Serial.print("Azimuth :\t"); Serial.print(azimuth);Serial.print("\t"); Serial.print(headingDegrees);Serial.print("\t"); Serial.println(myAziOutput);
Serial.print("Altitude:\t"); Serial.print(altitude);Serial.print("\t"); Serial.print(currentPitch);Serial.print("\t"); Serial.println(altOutput);
Serial.print("Phase:   \t"); Serial.print(phase);Serial.print("\t"); Serial.print(phase);Serial.print("\t"); Serial.println(phase);//error look at
Serial.println(moonCoordString);
Serial.println(azimuth100);
Serial.println();//new line....

}




////fix the wraparound issue
/// decide between lbrary and own code

int PID_Control_Loop(int desire, int reality, float KP){
  //Takes: goal. feedback, constant for proportional control
  // returns: signed output to run motor at. -255,0,255
  //includes deadband for easyness. TO be removed later.
  //No I no D at the moment
  //!!!!
//++++ check if err less -180 error+=360
// !!!
  int error = desire - reality;
  int output = KP * error; //KP is my constant tuned thing for proportional contol
  output = int(output);//analogWrite is int only

int outputMaxMagnitude = 255;
if (output > outputMaxMagnitude){
    output = outputMaxMagnitude;
  }
  
if (output < -outputMaxMagnitude){
    output = -outputMaxMagnitude;
  }
  int angularEpsilon = 4; //3* is acceptable at this stage
  int fullCirlce = 360;
  if (!almostEquals(desire,reality,angularEpsilon)){//if not within deadband
    if (abs(error)>fullCirlce/2){
      return -output;
    }
    else{ 
      return output ;             
    }
  }
  else{
    return 0 ;
  }
}

/*

probematic^^^^



*/


boolean almostEquals(int x, int y,int epsilon){
  //checks to see if two integers are near eachother
  return(abs(y-x)<=epsilon);
}








