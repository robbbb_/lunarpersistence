# smshandler.py
import logging
from testeSpeak import speak

class TALKHandler(logging.Handler): # Inherit from logging.Handler
        def __init__(self):
                # run the regular Handler __init__
                logging.Handler.__init__(self)
                # Our custom argument
#                self.phonenumber = phonenumber
        def emit(self, record):
                # record.message is the log message
                speak(record.message)