# -*- coding: utf-8 -*-
#UTF-8 allows for the ° symbol. It is a good symbol.


import sys
import os
import time
import smbus
import math
from datetime import datetime, date
from Adafruit_I2C import Adafruit_I2C


# from i2clibraries import i2c old crap hate
from time import *

class i2c_hmc5883l:
    
    ConfigurationRegisterA = 0x00
    ConfigurationRegisterB = 0x01
    ModeRegister = 0x02
    AxisXDataRegisterMSB = 0x03
    AxisXDataRegisterLSB = 0x04
    AxisZDataRegisterMSB = 0x05
    AxisZDataRegisterLSB = 0x06
    AxisYDataRegisterMSB = 0x07
    AxisYDataRegisterLSB = 0x08
    StatusRegister = 0x09
    IdentificationRegisterA = 0x10
    IdentificationRegisterB = 0x11
    IdentificationRegisterC = 0x12
    

    MeasurementContinuous = 0x00
    MeasurementSingleShot = 0x01
    MeasurementIdle = 0x03
    
    def getPiRevision(self):
        "Gets the version number of the Raspberry Pi board"
            # Courtesy quick2wire-python-api
            # https://github.com/quick2wire/quick2wire-python-api
        try:
          with open('/proc/cpuinfo','r') as f:
            for line in f:
              if line.startswith('Revision'):
                return 1 if line.rstrip()[-1] in ['1','2'] else 2
        except:
          return 0

    
    def __init__(self, addr=0x1e, gauss=1.3, debug= False):
#       self.bus = i2c.i2c(port, addr)#original opld crap
        self.i2c_mag = Adafruit_I2C(addr, smbus.SMBus(1 if self.getPiRevision() > 1 else 0), debug)
        self.setScale(gauss)#original
        self.address_mag = addr
        self.debug = debug
        self.setDeclination(9,18)
#        self.setContinuousMode()
        

        
    def __str__(self):
        ret_str = ""
        (x, y, z) = self.getAxes()
#        ret_str += "Axis X: "+str(x)+"\n"       
#        ret_str += "Axis Y: "+str(y)+"\n" 
#        ret_str += "Axis Z: "+str(z)+"\n" 
        
#        ret_str += "Declination: "+self.getDeclinationString()+"\n" 
        
        ret_str += "Heading: %.3f"%self.getHeadingString() 
        
        return ret_str
        
        
        
    def setContinuousMode(self):
        self.setOption(self.ModeRegister, self.MeasurementContinuous)
        
    def setScale(self, gauss):
        if gauss == 0.88:
            self.scale_reg = 0x00
            self.scale = 0.73
        elif gauss == 1.3:
            self.scale_reg = 0x01
            self.scale = 0.92
        elif gauss == 1.9:
            self.scale_reg = 0x02
            self.scale = 1.22
        elif gauss == 2.5:
            self.scale_reg = 0x03
            self.scale = 1.52
        elif gauss == 4.0:
            self.scale_reg = 0x04
            self.scale = 2.27
        elif gauss == 4.7:
            self.scale_reg = 0x05
            self.scale = 2.56
        elif gauss == 5.6:
            self.scale_reg = 0x06
            self.scale = 3.03
        elif gauss == 8.1:
            self.scale_reg = 0x07
            self.scale = 4.35
        
        self.scale_reg = self.scale_reg << 5
        self.setOption(self.ConfigurationRegisterB, self.scale_reg)
        
    def setDeclination(self, degree, min = 0):
        self.declinationDeg = degree
        self.declinationMin = min
        self.declination = (degree+min/60) * (math.pi/180)
        
    def setOption(self, register, *function_set):
        options = 0x00
        for function in function_set:
            options = options | function
        # self.bus.write_byte(register, options)
        self.i2c_mag.write8(register, options)
        
    def getDeclination(self):
        return (self.declinationDeg, self.declinationMin)
    
    def getDeclinationString(self):
        return str(self.declinationDeg)+"\u00b0 "+str(self.declinationMin)+"'"
    
    # Returns heading in degrees and minutes
    def getHeading(self):
        (scaled_x, scaled_y, scaled_z) = self.getAxes()
#        (scaled_x, scaled_y, scaled_z) = (float(scaled_x), float(scaled_y), float(scaled_z))
        headingRad = math.atan2(scaled_y, scaled_x)
        headingRad += self.declination

        # Correct for reversed heading
        if(headingRad < 0):
            headingRad += 2*math.pi
            
        # Check for wrap and compensate
        if(headingRad > 2*math.pi):
            headingRad -= 2*math.pi
            
        # Convert to degrees from radians
        headingDeg = headingRad * 180/math.pi
        return headingDeg
#        degrees = math.floor(headingDeg)
#        minutes = round(((headingDeg - degrees) * 60))
#        return degrees
#        return (degrees, minutes)
    
    def getHeadingString(self):
        self.getHeading()
        return self.getHeading()
#        (degrees, minutes) = self.getHeading()
#        return str(degrees)+"\u00b0 "+str(minutes)+"'"
        
    def getAxes(self):
        ##
        # self.i2c_accel.readU8(
            # self.i2c_mag.write8(rtegisterer, cvontents)
        ##
#        print " XXX:" ,bin(self.i2c_mag.readU8(self.AxisXDataRegisterLSB)), bin(self.i2c_mag.readU8(self.AxisXDataRegisterMSB))
        magno_x = self.i2c_mag.readS16Robb(self.AxisXDataRegisterMSB,self.AxisXDataRegisterLSB)
        magno_z = self.i2c_mag.readS16Robb(self.AxisZDataRegisterMSB,self.AxisZDataRegisterLSB)
        magno_y = self.i2c_mag.readS16Robb(self.AxisYDataRegisterMSB,self.AxisYDataRegisterLSB)
#        magno_x = self.i2c_mag.reverseByteOrder(magno_x)
#        print 'magnoX ', magno_x
#        print 'msbX: %x lsbX: %x' % (self.i2c_mag.readU8(self.AxisXDataRegisterMSB), self.i2c_mag.readU8(self.AxisXDataRegisterLSB))
#        (x,y) = (self.i2c_mag.readU8(self.AxisXDataRegisterMSB), self.i2c_mag.readU8(self.AxisXDataRegisterLSB))
#        magno_z = self.i2c_mag.readS16(self.AxisZDataRegisterMSB)
#        magno_y = self.i2c_mag.readS16(self.AxisYDataRegisterMSB)

        # (magno_x, magno_z, magno_y) = self.bus.read_3s16int(self.AxisXDataRegisterMSB)

        if (magno_x == -4096):
            magno_x = None
        else:
            magno_x = round(magno_x * self.scale, 4)
            
        if (magno_y == -4096):
            magno_y = None
        else:
            magno_y = round(magno_y * self.scale, 4)
            
        if (magno_z == -4096):
            magno_z = None
        else:
            magno_z = round(magno_z * self.scale, 4)
            
        return (magno_x, magno_y, magno_z)
        
    
if __name__ == "__main__":
    compass = i2c_hmc5883l()
    compass.setContinuousMode()
    while 1:
        print compass.getHeading()
        sleep(0.666)