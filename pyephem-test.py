# -*- coding: utf-8 -*-
'''
Robb Godshaw - PyEphem proof of viability - Heavily edited example code
https://pypi.python.org/pypi/pyephem/
http://rhodesmill.org/pyephem/
'PyEphem provides scientific-grade astronomical computations for Python'

--Extracts clean data regarding the precise apparent location of the moon--

Inputs: Time, Lattitude, Longitude, Altitude(above sea level)
Outputs: Apparent Azimuth, Altitude(above horizon) and %illumination of the moon
'''

import ephem
import math

#Info about observer. currently hard coded to dohery hall
#to be fed from GPS later on.

robb = ephem.Observer()
#robbStudioDecimalCoordLonFirst= ('-79.94493333333333','40.44251666666667')
robb.lon, robb.lat = '-79.94493333333333','40.44251666666667'
# robb.date = '2013/12/18 00:19:12'

moon = ephem.Moon()
moon.compute(robb)

#convert the radians to more-usefull degrees
LunarAltitude = math.degrees(float(moon.alt))
LunarAzimuth = math.degrees(float(moon.az))

print 'Current lunar position over Robb(Doherty Hall) is %.3f°Alt, %.3f°Az'\
% (LunarAltitude,LunarAzimuth)
print 'Current lunar illumination is %d%% ' % (moon.phase)
print 'current Universal Time: %s'%(robb.date)


####Below this line is scratch paper--------

# ephem.moon_radius
# speedOfLight = ephem.c
#moon.moon_phase,

def phaseCalc(now,lastFull,nextFull):
    #an unfinished function to calculate the lunar phase in more useful terms
    ''' returns %% illuminated of moon at time'''
    period = nextFull- lastFull
    faralong = (now - lastFull) / period
    return faralong


