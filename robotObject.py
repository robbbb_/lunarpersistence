# -*- coding: utf-8 -*-
#UTF-8 allows for the ° symbol. It is a good symbol.
'''
Robb Godshaw - Robot Object - 100% homemade code.
all references are soley algotitmic and cited.
See adjacent files for library info
This object holds, calculates, returns, and handles all robot variables

NOTE--THIS CODE WILL ONLY RUN ON A RASPBERRY PI!
'''
import time
import math #radian conversion and general trig
from datetime import datetime, date
from Adafruit_LSM303DLHC import LSM303DLHC #Adafruit library for compass
#from Adafruit_ADS1x15 import ADS1x15 #adafruid library for analog to dig chip

from motor import motor #I wrote this one! OOP for the win!! :-)
from newCompass import i2c_hmc5883l #I sorta wrote this one


class robot(object):
    """when called, this populates itself with the intrisics and variables 
    pertaining to the giant robots mechanical facilities
    mostly pitch and true heading"""
    def __init__(self):
        super(robot, self).__init__()
        #self.panMotor=motor('pan')
#        self.lsm = LSM303DLHC(0x19, 0x1E, False)
#        self.lsm.setTempEnabled(True)
#        self.compass = i2c_hmc5883l()
#        self.compass.setContinuousMode()
        self.heading = None
        self.pitch = None
        self.gpsConnected = False

        self.update()

    def update(self):
        self.fetchLocation()
#        self.fetchRobotHeading()
        self.fetchRobotPitch()
#        self.fetchTemp()
    def compassInit(self):
        pass

    def fetchTemp(self):
        self.temp = self.lsm.readTemperatureCelsius()

    def fetchRobotHeading(self):
        #self.heading = convertHeading(self.lsm.readMagneticHeading())
#        self.heading = self.compass.getHeading() 
        self.heading = None

    def fetchRobotPitch(self):
        self.getPitchVoltage()
        minVoltage = 3567
        maxVoltage = 2000
        minPitch   = -90
        maxPitch   = 90
        pitch =maddyMap(self.pitchVoltage,minVoltage,\
        maxVoltage,minPitch,maxPitch)
        if pitch>maxPitch:pitch=maxPitch
        if pitch<minPitch:pitch=minPitch
        self.pitch = pitch

    def getPitchVoltage(self):
        gain = 4096  # +/- 4.096V
        sps = 250  # 250 samples per second
        ADS1115 = 0x01    # 16-bit ADC
        adc = ADS1x15(ic=ADS1115)
        pitchEncoderPin = 0
        headingEncoderPin = 1
        self.pitchVoltage= adc.readADCSingleEnded(pitchEncoderPin, gain, sps)
        self.headingEncoderPin = adc.readADCSingleEnded(headingEncoderPin, gain, sps)

    def fetchLocation(self):
        if not self.gpsConnected:
            #gps signals do not penetrate into the sub basements of doherty hall
            self.lon, self.lat = '-79.9449','40.4425' #from google maps
        else:
            pass
            #GPS stuff to be called here later on...
            #not finished in time
            #getGPS_Location()
            #robotData.botLocation = robotData.GPS_Location
        
    def __str__(self):
        toReturn='Current robot Heading is %s (%.1f° clockwise from magnetic north)'% ('UNKNOWN GET OVER IT.', 666.666)
#        (headingToCardinal(self.heading),self.heading) + \
        toReturn += '\nCurrent robot Pitch is %s(%.1f° %s horizon)' %\
       (pitchToVernacular(self.pitch),self.pitch,\
         ('above' if self.pitch>0 else 'below')) 
#         '\nCurrent robot illumination is %.2f%% ' % 999.9999
        return toReturn

def convertHeading(heading):
    #not in use...
    '''takes -180°+180° headings, returns 0°,+360° clockwise degrees from N'''
    fullCircle = 360
    return (heading if heading>=0 else fullCircle +heading)
    

def maddyMap(input,minn,maxx,low,high):
    #maddy varner(mvarner) helped with this code
    low,high = min(low,high),max(low,high)
    minn,maxx = min(minn,maxx),max(minn,maxx)
    outSpan = (maxx - minn)
    if outSpan ==0 :return 0
    return (((input - minn) * (high - low)) / (outSpan)) - low

def maddyMapTest():
    print 'testing your mapp function...',
    assert (maddyMap(33,0,99,0,9) == 3)
    assert (maddyMap(5,0,10,0,100) == 50)
    print
    assert (maddyMap(5,0,10,10,0) == 5)
    assert (maddyMap(5,0,10,0,10) == 5)
    assert (maddyMap(5,0,0,0,0) == 0)
    assert (maddyMap(0,0,0,0,0) == 0)
    print 'passed!'
def testconvertHeading():
    print 'testing convertHeading....',
    assert convertHeading(1) == 1
    assert convertHeading(0) == 0
    assert convertHeading(-90) == 270
    assert convertHeading(33) == 33
    assert convertHeading(-100) == 260
    print 'passed!'

def rollingAverage(datum, depth):
    '''takes a new value and returns the average of 
    it and its (depth-1) predeseccors'''
    pass
    #incomplete, perhaps uneeded

def testRollingAverage():
    pass
    pass

def headingToCardinal(heading):
    fullCirlce=360.0
    cardinalQty =16
    wedgeSize = fullCirlce /cardinalQty
    offset = wedgeSize/2.0
    cardinalIndex = int(((heading+offset)%fullCirlce)/wedgeSize)
    cardinalList = \
    ['N','NNE','NE','ENE','E','ESE','SE','SSE',\
     'S','SSW','SW', 'WSW','W','WNW','NW','NNW']
    return cardinalList[cardinalIndex]

def pitchToVernacular(pitch):
    fullCirlce=360.0
    halfCircle = fullCirlce/2
    quarterCirle = halfCircle/2
    cardinalQty =16
    wedgeSize = fullCirlce /cardinalQty
    offset = wedgeSize/2.0
    angleIndex = int(((pitch+quarterCirle+offset)%fullCirlce)/wedgeSize)
    angleList = [
    'Directly underfoot',      #-90
    'Not quite directly underfoot',
    'Way below horizon',#-45°
    'below horizon',
    'At horizon', #------0°------
    'Above horizon',
    'High',#45°
    'Not quite Directly overhead',
    'Directly overhead',#90
    ]
    return angleList[angleIndex]

def testPitchToVernacular():
    print 'testing pitchToVernacular...',
    assert pitchToVernacular(90) ==     'Directly overhead'
    assert pitchToVernacular(89) ==     'Directly overhead'
    assert pitchToVernacular(88) ==     'Directly overhead'
    assert pitchToVernacular(  60) ==     'Not quite Directly overhead'
    assert pitchToVernacular(  45) ==     'High'
    assert pitchToVernacular(  13) ==     'Above horizon'
    assert pitchToVernacular(  5) ==     'At horizon'
    assert pitchToVernacular(  0) ==     'At horizon'
    assert pitchToVernacular(  -5) ==     'At horizon'
    assert pitchToVernacular(  -15) ==     'below horizon'
    assert pitchToVernacular( -45) ==     'Way below horizon'
    assert pitchToVernacular( -70) ==     'Not quite directly underfoot'
    assert pitchToVernacular( -89) ==     'Directly underfoot'
    assert pitchToVernacular( -90) ==     'Directly underfoot'
    print 'Passed!'

def testHeadingToCardinal():
    print 'testing headingToCardinal...',
    assert headingToCardinal(0) == 'N'
    assert headingToCardinal(359) == 'N'
    assert headingToCardinal(358) == 'N'
    assert headingToCardinal(1) == 'N'
    assert headingToCardinal(3) == 'N'
    assert headingToCardinal(11) == 'N'
    assert headingToCardinal(13) == 'NNE'
    assert headingToCardinal(180) == 'S'
    assert headingToCardinal(190) == 'S'
    print 'Passed!'

def testAll():
    testHeadingToCardinal()
    testPitchToVernacular()
    testconvertHeading()


def almostEquals(x,y,epsilon):
    return(abs(y-x)<epsilon)

##test stuff below
#maddyMapTest()
rrr= robot()
print rrr
time.sleep(1)
rrr.update()
print rrr


