#!/usr/bin/perl
##/usr/share/adafruit/webide/repositories/lunarpersistence/IPspeak.pl
#  IPspeak.pl

# speak the IP address of this machine, e.g. for a Raspberry Pi with no screen.
# requires "espeak" (sudo apt-get install espeak)

# Andy Stanford-Clark (@andysc)
# 18-May-13

# give it a chance to get an IP address at boot-up
sleep 10;

while (1)
{
  # look at wireless first, and say that, even if there's also ethernet
  $IP = `ifconfig wlan0 | grep 'inet addr'`;
  $text = "wireless";

  if (!$IP)
  {
    # if no wireless, look for ethernet
    $IP = `ifconfig eth0 | grep 'inet addr'`;
    $text = "ethernet";
  }

  if ($IP =~ /inet addr:(.*?) /)
  {
    # insert spaces between all characters
    @characters = split(//,$1);
    $text .= " address " . join(" ",@characters);

    # change dots to, um, dots
    $text =~ s/\./dot/g;
  }
  else
  {
    $text = "no IP address";
  }

  print "'$text'\n";
  system("espeak '$text'");

  # wait a minute before doing it again
  sleep 60;
}
