'''
Robb Godshaw - PyGame proof of viability - this whole file is my work.
'pygame is a decent graphics library'

--Displays the moon in its proper phase for debugging and aesthetics--

Inputs: Time, Lattitude, Longitude, Altitude, illumination of moon
Outputs: graphical representation of:
Apparent Azimuth, Altitude(above horizon) and %illumination of the moon
'''
import pygame
import time
#from pyscope import pyscope

pygame.init()
w = 640
h = 480
size=(w,h)
screen = pygame.display.set_mode(size)

def moon():
        # Fill the screen with red (255, 0, 0)
        moonColor = (255, 255, 255)
        spaceColor = (1, 1, 1)
        #scope.screen.fill(spaceColor)
        #pygame.draw.circle(self.screen, moonColor,  (100,100) ,100,0)
        
        moon = pygame.image.load('moonbrite.gif').convert()
        #scope.
        screen.blit(moon, (100, 50))
        # Update the display
        pygame.display.update()
 
# Create an instance of the PyScope class
#scope = pyscope()
while(1):
        moon()
