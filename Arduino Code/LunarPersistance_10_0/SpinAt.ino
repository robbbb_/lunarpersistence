/*
SpinAt houses the function that spins the motors.
 Pin assignments, safety checks, 
 
 */

//define all the control pins
//Motor A
int enableA = 14; //Enables for speed
int motorAIn1 = 16; //Ins for direction
int motorAIn2 = 17;

//Motor B
int motorBIn3 = 18;
int motorBIn4 = 19; 
int enableB = 15;
int pinA, pinB;


void motorSetup(){
  //initialize all the control pins as outputs
  Serial.println("Motor Control Initialized.");
  pinMode(motorAIn1, OUTPUT);
  pinMode(motorAIn2, OUTPUT);
  pinMode(enableA, OUTPUT);
  pinMode(motorBIn3, OUTPUT);
  pinMode(motorBIn4, OUTPUT);
  pinMode(enableB, OUTPUT);
}

void spinAt( int whichMotor, int speed0255){ //Function to spin the motor at a speed


if (speed0255 > 255){
    speed0255 = 255;
  }
  
if (speed0255 < -255){
    speed0255 = -255;
  }
  //0 is azi, pan
//  Serial.print( "Attempting to spin "); 
//if (whichMotor == 0){
//  Serial.print ("pan motor");
//}
//else{
//    Serial.print ("tilt motor");
//}
//  Serial.print( " at a speed of"); Serial.println (speed0255);
  int pinToPwm;
  int pinToLow;
  int pinToHigh;
  //String name;

  if (whichMotor==0){
    pinA = motorAIn1;
    pinB = motorAIn2;
    pinToPwm = enableA;

  }
  else if (whichMotor==1){
    pinA = motorBIn3;
    pinB = motorBIn4;
    pinToPwm = enableB;
  }
  else{
    ///BROKEN
  }
  if (speed0255 ==0){
    brake();
  }
  else if (speed0255 > 0){
    clock();
  }
  else {
    anticlock();
  }

  
  

  analogWrite(pinToPwm, abs(speed0255));
}

void clock(){
  digitalWrite(pinA, LOW);
  digitalWrite(pinB, HIGH);
}

void anticlock(){
  digitalWrite(pinA, HIGH);
  digitalWrite(pinB, LOW);
}

void brake(){
  digitalWrite(pinA, LOW);
  digitalWrite(pinB, LOW);
}

void safetyCheck(){
//  if (whichMotor==1){
    if(pitchEncoder>dangerHigh){
      Serial.println("TOO HIGH");
      //      speed0255 = 155;
      spinAt(1,-255);


    }
    if(pitchEncoder<dangerLow){
      Serial.println("TOO LOOO");
      //      speed0255 = -155;
//      clock();
      spinAt(1,255);
    }
  }




