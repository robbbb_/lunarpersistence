LSM303 compass;
void compassSetup(){
  Serial.print("compassSetup...");

  compass.init();
  compass.enableDefault();

  /*
  Calibration values; the default values of +/-32767 for each axis
   lead to an assumed magnetometer bias of 0. Use the Calibrate example
   program to determine appropriate values for your particular unit.
   */
//  compass.m_min = (LSM303::vector<int16_t>){-32767, -32767, -32767};
//  compass.m_max = (LSM303::vector<int16_t>){+32767, +32767, +32767};
//  
  compass.m_min = (LSM303::vector<int16_t>){  -500,    -77,   -500};
  compass.m_max = (LSM303::vector<int16_t>){  +200,   +800,   -450};




  Serial.println("complete!");
}

void getHeading() 
{
  compass.read();
  headingDegrees = (int(compass.heading())+numberPotValue)%360;


}


