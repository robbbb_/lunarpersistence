/*
Some pasted code from an earlier joystick example
 
 not complete
 not functional at all
 
 
 */
 
// int joyAPin = 1;
//int joyBPin = 0;
////int pwrJoy = 18;
////int gndJoy = 19;
//
//void joystickControl(){
//int pitchMag = getJoyMagnitudes(analogRead(joyAPin));
//int aziMag=getJoyMagnitudes(analogRead(joyBPin));
// spinAt();
//}


int getJoyMagnitudes( int signal){
  int center = 512; //resting reading from analog stick
  int maxx=1024;
  int deadBandWidth = 200; //how much play to have for safety
  int lowDeadBound = center - deadBandWidth;
  int hiDeadBound = center + deadBandWidth;
  boolean dead = (signal > lowDeadBound && signal < hiDeadBound);
  if (signal > lowDeadBound && signal < hiDeadBound){
    return 0;
  }
  else{
    return map(signal,0,maxx,-144,144);
  }
}

